The Corner Stone
----------------

	Psalm 118:22-23

	22 The stone which the builders rejected has become the head stone of 
	the corner. 23 This is the LORD'S doing; it is marvelous in our eyes.	

	Genesis 11:3-4

	3 And they said one to another, Come, let us make brick, and burn them 
	thoroughly. And they had brick for stone, and bitumen had they for mortar.
	4 And they said, Come, let us build us a city and a tower, whose top may 
	reach unto heaven; and let us make us a name, lest we be scattered abroad 
	upon the face of the whole earth.

	Matthew 21:42-44

	42 Jesus said unto them, Did you never read in the scriptures, The stone 
	which the builders rejected, the same has become the head of the corner: 
	this is the Lord's doing, and it is marvelous in our eyes? 43 Therefore 
	I say unto you, The kingdom of God shall be taken from you, and given to 
	a nation bringing forth the fruits thereof. 44 And whosoever shall fall 
	on this stone shall be broken: but on whomsoever it shall fall, it will 
	grind him to powder.

	Isiah 8:14-15

	14 And he shall be as a sanctuary; but a stone of stumbling and a rock of 
	offense to both the houses of Israel, for a trap and for a snare to the 
	inhabitants of Jerusalem. 15 And many among them shall stumble, and fall, 
	and be broken, and be snared, and be taken.

	Daniel 2:35

	35 Then the iron, the clay, the bronze, the silver, and the gold were 
	crushed together, and became like the chaff of the summer threshing floors; 
	and the wind carried them away, so that no place was found for them: 
	and the stone that struck the image became a great mountain, and filled 
	the whole earth.
